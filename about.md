---
layout: post
permalink: /about/index.html
title: A propos du blogeur
description: Apprenez-en un peu plus sur moi !
tags: [about, Jekyll, theme, responsive]
image:
  feature: baptiste-arkose-violet-bar.jpeg
---

Ce n'est jamais facile de se décrire. Je ne sais pas par où commencer.  
Déjà je suis heureux d'avoir enfin concrétiser mon souhait de tenir un blog ! J'avais cette idée en tête depuis plus d'un an.

Comme je le dis sur ce que j'appelle [mon landing site](https://www.baptiste-dauphin.com/#about), je m'appelle Baptiste Dauphin, né en 1994, au jour où j'écris ce post je viens d'avoir 26 ans. J'habite à Rouen en Normandie.

Professionnellement je suis Administrateur Systèmes et Réseaux ou "SysAdmin". Cela n'a pas l'air sexy dit comme ça, mais ce job me permet d'être très créatif et d'exprimer mes idées. Même si il ne s'agit que de serveurs informatique virtuels ou de notions encore plus abstraites, créer de la communication entre ces machines de façon orchestrée , chorégraphiée et organisée de telle façon à ce que quand une soit dans un mauvais état de santé les autres soient au courant et arrête de lui fournir de la charge de travail , je trouve ça très vivant en fin de compte !

De façon générale, je suis intéréssé par beaucoup de domaines et je m'émerveille facilement.  
A l'heure d'internet haut débit, de la vulgarisation de tous types sur YouTube ou encore grâce aux services permettants la recherche d'articles et de [publications scientifiques](https://scholar.google.fr/scholar?hl=fr&as_sdt=0%2C5&q=global+warming+causes&btnG=&oq=global+war), je me régale à passer du temps à apprendre et comprendre le monde. Mais si je devais donner des exemples de ce à quoi je consacre du temps, les voici ! 

- Sciences et Philosophie des sciences
- Psychologie (biais humains entre autre)
- Rhétorique
- Economie et finances
- Communication non-violente (qui a en bonne partie transformé ma vie, j'ai prévu d'écrire à ce sujet)
- Corps humain
- OpenSource (informatique en plus de mon job)
- Mécanique

Je fait aussi du sport. Je pratique 2 à 3 fois par semaines l'escalade de block. Les murs ne montent pas à plus de 5 mètres ! Pas besoin de baudrier, de corde ou de casque, uniquement des chaussons ! C'est donc de l'escalade très tonique plus en force qu'en escalade classique. 

![Drawing A]({{ site.url }}/images/block.jpeg)
