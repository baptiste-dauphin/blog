----
20 mars 2020
----

## Accepter l'incertitude
Est ce que nous sommes capables de vivre dans l'incertitude ? Nous avons organisés des vie tellement programmées où on se garantie de tout. Où on est hyper prudent, tout est couvert, par des lois, des règlements que tout d'un coup rencontrer de l'incertitude c'est très nouveau. Or c'est le principe même de la vie, nous ne connaissons ni l'heure ni le jour de notre fin dernière et nous sommes livrés à l'incertitude. 
Est ce que c'est angoissant ? Ou bien est ce qu'on le vit comme la nature même de ce que l'on a à vivre, avec ouverture, avec confiance. 

Je peux voir que nos vies perdent en saveur lorsqu' on veut, tout comprendre, tout savoir, tout maîtriser. C'est un peu comme un voyage organisé, où on sait exactement où on mange, à quelle heure on mange et ce que l'on mange. C'est peut être sécurisant, mais c'est assez ennuyeux. À l'inverse si on fait une balade en forêt ou en montagne sans trop savoir où on posera sa tante et si y aura de l'orage ou pas et si on arrivera à faire chauffer son petit feu pour faire sa tambouille je pense que la vie a plus de piment.

Je pense que nous pouvons nous encourager à dépasser notre zone de confort à accepter l'incertitude comme un des éléments de la vie. Et nous pouvons de plus en plus établir en nous cette confiance qu'il y a quelque chose en moi qui est vivant quoi que je traverse. Qui est hors d'atteinte du temps, hors d'atteinte des blessure de mon histoire.

## Commencer par vivre en couple avec soi-même
Beaucoup de personnes ont cette croyance sur elle qu'elles sont incapables de vivre seule, donc elle vont au travail, elles rentres chez elles vite se changer, elles ressortent prendre un verre avec des copains, elles vont voir un spectacle, elles vont en soirée. Et qui ont vraiment du mal à être seules plusieurs jours. 
C'est vraiment difficile d'être en lien profond avec qui que se soit si on arrive pas à être en lien profond avec soi. Et que le premier couple, par exemple si on se rêve d'être en couple, c'est de faire un couple avec soi-même. J'ai pu voir beaucoup de personnes qui sont en difficulté à former un couple à avoir une relation amoureuse durable. Souvent ce sont des personnes qui n'arrivent pas à être seul avec elle-même et qui cherchent une *moitié*. Or l'autre n'est pas là pour me consoler de n'être qu'à moitié moi-même, l'autre est là pour partager ma plainitude ! Et l'on co-créé de l'enchantement.