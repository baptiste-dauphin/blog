Tropisme de la pensée occidentale est de penser que la crise que nous traversons est une meta-crise qui necessairement implique la totalité du monde.
Cette crise est très grave, il ne faut pas la relativiser au sens "la prendre à la légère", ca serait une faute. Mais il faut la relativiser au sens que l'on ne peut pas oublier que :

chaque année :
- 2 500 000 personnes meurent d'infection respiratoires.
- 9 000 000 personnes meurent de faim. 
- 9 000 000 personnes meurent de la polution.

Il est maintenant de notre rigueur intellectuelle que de comprendre que le monde est inervé de crises dramatiques.
Il y a quelques jours encore : des criquets qui dévastent là encore des pays pauvres en induisant vraisemblablement des épisodes de famines considérable dans la plus grande indifférence.

Il serait peut-être temps d'évaluer les situations un peu au delà du prisme de l'immédiateté de leur effet sur les locuteurs, c'est à dire nous.

On ne peut pas raisonablement tout faire pour détruire l'hopital et les services public et s'étonner qu'ils fonctionnent moins bien. Nous devons nous rendre compte que nous n'en arrivons pas là par hasard. On ne peut pas soutenir une politique de destruction du commun et appeler, scandalisé, à un meilleur commun lorsqu'on en a besoin. Cela ne peut pas se résoudre sans anticipation. C'est très exactement ce dont nous sommes incapable.
On le voit avec la meta-crise écologique. Qui sera bien plus grave que la crise du covid-19. Qui a été annoncé par tous les spécialistes compétent. Qui fait l'hunanimité chez les gens qui ont étudiés la question qui ont les connaissances biologiques et climatiques pour comprendre ce qui se passe. 
On parle de Guerre, on parle de famine généralisée (déjà en recrudescence) et nous n'anticipons pas. J'espere que nous allons en tirer les leçons et apprendre.

Il n'y a pas une source distincte dans cette épidémie mais,
les spécialistes montrent aujourd'hui que vraissemblablement aujourd'hui la fonte des espaces de vies pour les animaux, qui est incroyablement rapide et qui fait que des espèces qui vivaient séparées commencent à se toucher est en train de contribuer à l'émergence de nouvelles pandémies parce que les agents patogènes passent beaucoup plus facilement la barrière des espèces.
