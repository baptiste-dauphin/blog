---
layout: post
title:  "Jour 1 (Anniversaire)"
date: Mardi 16 mars 2020
categories: jekyll update
---

### Journée

8h30 : Réveil tardif avec une grosse paresse.
9h00 : Début du télé-travail sur mon ordinateur portable installé sur le bureau à côté de mon lit. Je n'ai même pas encore pris mon petit-déjeuner.

12h30 : Le midi, j'ai commandé à manger car avec l'anticipation des du confinement à venir (j'ai enttendu parler de 45 jours par un collègue) je me sentais vider de mes forces et me faire à manger était trop couteux.  

Le soir j'ai arrêté le travail à 18h05, dansé un peu par envie et aussi pour me préparer aux festivals de cet été.  


Je n'ai pas joué aux jeux vidéos. Pourtant ça fait 4 semaines que je n'y joue pas alors qu'une mise à jour de Monster Hunter vient de sortir ! Mais je sens que j'ai mieux à faire. Mon site web que j'ai démarré la semaine passé mérite encore quelque corrections, je ne veux pas laisser ça en plan ! Ma description ne me correspondait pas completement et une des photos est à l'envers. J'ai donc découvert un logiciel que maintenant j'adore (*Gimp*) un logiciel libre et gratuit qui réalise les mêmes fonctions que PhotoShop. J'aime, ça me rappel ma période du lycée ou je m'était lancé en auto didacte dans la retouche photo et au montage vidéo. Ma partie nostalgique était rassasiée.

Ma mère m'a appelé 3 fois dans la journée à chaque fois de plus en plus inquiétée pour moi parce que je vit seul. Sauf que je ne stresse pas et que c'est elle qui transmet son angoisse à son environement.

20h00 : Annonce du président qui nous annonce que tous les Français vont devoir rester confiner chez eux pour une période de minimum 15 jours. Pendant le discours, je me sens connecter avec tous les Français, nous avons un énnemi et un but commun, arrêter la propagation du virus pour retrouver la liberté au plus vite !

Le soir après avoir dansé quelque minutes je me suis agréablement surpris en ayant finalement l'envie de  cuisiner.

22h00 : Je me demande quel impact aura cette période d'isolement, sans beaucoup de contact social autre que par vidéo, voix et message. Je vis en appartement dans le centre d'une grande Ville depuis plusieurs année, donc surement que je ne verrai pas un grand changement. Ou bien peut être que si, au contraire ! Je ne m'en aperçois peut-être pas, mais avec le temps, ma routine est  bien rodée, j'ai mon équilibre, entre soirée où je reste seul volontairement et soirée sportive à la salle d'escalade ou encore les repas chez ma grand-mère. En cette première journée, je ne ressens aucunes vraies différences avec un jour classique de télé-travail. Je suis interogatif et attentif à la suite !

00h00 : Cette journée me faisait peur après l'annonce du président de jeudi et celle du premier ministre samedi, mais en fin de compte je me suis surpris à m'adapter en trouvant des opportunités à ce confinement obligatoire : lire, cuisiner, danser, ecrire, laisser aller ses pensées en étant oisif (ca fait tellement de bien), reprendre contact avec des amis.
