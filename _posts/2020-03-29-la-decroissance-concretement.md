---
layout: post
title: "La décroissance concrètement"
description: "On court à la CATASTROPHE. On détruit la planète un peu plus chaque jour. Merci ça y est, je crois qu'on a compris. Mais concrètement comment faire pour endiguer ce problème et faire face à la meta-crise qui arrive ? Est-ce aux industriels d'agir ? Est-ce à l'état ? Est ce qu'un citoyen sans aucun pouvoir politique comme moi peut y changer quelque chose ?"
category: articles
tags: [décroissance, climat, économie]
image:
  feature: louisiana-forest-fire.jpg
---

Ce n'est plus un secret pour personne et rare sont ceux qui nient encore l'implication de l'être humain dans ce que l'on appelle : *la destruction du vivant*, *la méta-crise écologique*, *le réchauffement de la planète*, ou encore *notre rédemption* par certains ayants perdu fois en l'humanité.

On court à la catastrophe. On détruit la planète un peu plus chaque jour. Merci, ça y est, je crois qu'on a compris, on est tous au courant. Cela devient angoissant. Et c'est précisément ce qui __déclenche notre ignorance__ et qui fait que l'on passe à un autre sujet très rapidement.


Face à une situation qui *paraît* perdu d'avance, l'esprit humain déclenche un mécanisme d'auto-défense pour ne pas sombrer dans la folie. Sans ce mécanisme il pourrait apparaître des comportements irrationels tel qu'une consommation outrancière encore pire que maintenant ou encore des vagues de suicide collectifs. 

Ce mécanisme nous le connaissons tous il s'agit tout simplement du déni. Pour comprendre son fonctionnement nul besoin de connaissances approfondies en Psychanalyse. Je pense que la page wikipédia est suffisante.
> Le déni est l'attitude de refus de prendre en compte une partie de la réalité, vécue comme inacceptable par l'individu. En psychanalyse c'est un mécanisme de défense, par lequel le sujet refuse de reconnaître la réalité d'une perception ressentie comme menaçante et/ou traumatisante.

![Politique de l'autruche]({{ site.url }}/images/politique_autruche.webp)

Mais *grâce* à ce mécanisme il n'en est rien ! Alors nous restons en vie, mais à quel prix ? Au prix de n'avoir qu'une conscience biaisée des enjeux à venir. Qui nous conduira inexorablement à notre perte de manière certaine pour le coup ! Les scientifiques annoncent qu'à la fin du siècle : 70% de la population mondiale subira des canicules mortelles.

Quand j'ai commencé à accepter que nous courrons à la catastrophe si nous continuons de vivre de la même façon depuis les années 1960 en consommant sans réfléchir, j'ai pris peur. Je me suis senti perdu, abandonné, impuissant et lâché par la société.
Cette idée m'était insupportable ! Je ne pense pas que l'être humain est d'un naturel résigné, je pense que pour la plupart d'entre nous, nous nous sentons perdu, écrasé et oublié dans des villes trop grandes (où paradoxalement le sentiment de solitude enregistré est plus élevé qu'à la campagne). Alors mon esprit citoyen responsable a commencé à émerger. J'ai commencé à m'interroger et à remettre en cause.

Concrètement, comment faire pour endiguer ce problème et faire face à la méta-crise systémique qui arrive ? Est-ce aux industriels d'agir ? Est-ce à l'état ? Est ce qu'un simple citoyen comme moi a le pouvoir de changer quelque chose ? Ou bien est ce que mon action individuel n'est qu'une goutte d'eau dans l'océan ?

Ce que l'on peut faire tous à notre échelle individuelle et qui nous habitue à décroitre en terme de confort extrême et d'habitude de consommation outrancière.

## Actions
La plupart font économiser de l'argent.

- Utiliser le vélo ou les transports en commun.
- Acheter la nourriture en vrac.
- Acheter local.
- Diminuer progressivement sa consommation de viande et explorer de nouvelles saveurs.
- Réduire l'achat de vêtements.
- Diminuer les achats sur amazon.
- Chauffer à 19°C maximum.
- Mutualiser les déplacements.


### Utiliser le vélo ou les transports en commun.
Utiliser la voiture n'est pas interdit mais c'est à utiliser lorsque les transports en commun sont inutilisables. Pour les personnes vivants à la campagne c'est plus difficile de se passer de la voiture car les campagnes sont moins bien et ou moins souvent deservis par les réseaux de transports en commun. Dans ce cas on peut quand même essayer de mutualiser les déplacements en essayant de déposer plusieurs personnes avec un seul trajet en voiture.

Avantage à utiliser les transports en commun :
- Je suis moins stressé car pas concentré à conduire mon véhicule et pas coincé dans les bouchons. 
- J'ai le temps de lire tranquillement mon livre ou ma musique dans le métro / bus.

Et pour ce qui est du vélo :
- Beaucoup moins couteux que la voiture.
- Ne pollue pas du tout.
- Liberté totale des horaires.
- Possibilité de prendre des raccourcis inaccessibles en voiture. (valable surtout en ville).
- Opportunité géniale pour réaliser de l'exercice physique ! Gros apports sur le plan bien-être mental avec la prise de temps pour se laisser aller à ses idées et ses réfléxions pendant qu'on pédale ! 

Grosse économie d'argent : Essence, usure de la voiture, coûts des réparations qui vont avec, décote de la voiture.

### Acheter la nourriture en vrac.
Réduit considérablement les déchets plastiques, cartons, aluminium. Cela fait moins de déchets à recycler, trier, brûler.

### Acheter local.
Cela vous ferait plaisir de contribuer à la prospérité des agriculteurs de votre région ? Vous ne savez pas comment les soutenir ? En achetant leur produits. Dans ma ville (Rouen), certains services de livraison dédié entièrement au local existe. Vous pouvez taper "coopérative alimentaire dans ma ville" dans votre moteur de recherche préféré.
Pas besoin de transport traversant les océans, ou circulant par avions.
Très souvent de saisons donc meilleur en goût. Et si bio largement meilleur pour la santé.
Exemple sur Rouen / Normandie :
- [Bulkee shop](https://bulkee-shop.com/)
- [Alternoo](http://alternoo.fr/)
- [Le producteur local](https://leproducteurlocal.fr/)
- [Bienvenue à la ferme](https://www.bienvenue-a-la-ferme.com/)
- [Charcuterie à la ferme](https://www.charcuterie-a-la-ferme.com/)

### Diminuer progressivement sa consommation de viande et explorer de nouvelles saveurs.
Les chercheurs [Joseph Poore](https://www.linkedin.com/in/joseph-poore-53539041/) et [Thomas Nemecek](https://www.linkedin.com/in/thomas-nemecek-16042717/) respectivement chercheurs à l'université d'Oxford et ingénieur agronome ainsi que chef adjoint de [Agroscope](https://www.agroscope.admin.ch/agroscope/fr/home/publications/recherche-publications/agroscope-science.html) (centre de compétence de la Confédération suisse pour la recherche agricole) ont démontrés entre autre grâce à [cette étude](https://josephpoore.com/Science%20360%206392%20987%20-%20Accepted%20Manuscript.pdf) que :

> Plus de __80% des terres cultivées__ sont utilisées pour le *bétail* mais ne produisent que __18% des calories__ alimentaires et __37% des protéines__ de nos besoins.

Et conseillent de réduire drastiquement notre consommation de viande si nous voulons réduire la pollution que toute cette industrie rejette dans les airs.

![Drawing A]({{ site.url }}/images/vegan_burger.jpg)

Se passer de viande est beaucoup plus facile et acceptable qu'il n'y paraît ! Et je ne dis pas ça parce que j'ai pratiquement arrêter d'en consommer, je n'appréciais déjà plus vraiment la viande avant de tester le végétalisme. Donc je considère que je ne suis pas un bon exemple de repenti.

Il existe tout un pan de la nourriture que la plupart d'entre nous n'avons jamais essayé ! Découvrez encore plus de fruits, [légumineuse](https://www.consoglobe.com/2016-legumineuses-cg), laitages végétaux, oléagineux, céréales, pâtes, sauces et assaisonnements... il y a tellement de chose à faire découvrir à vos papilles !

Il existe pléthore de [conseils et d'astuces sur le net](https://www.qwant.com/?q=repas%20vegetalien&t=web) pour redecouvrir la cuisine sans viande.
Un exemple de regime vegetalien équilibré [unlockfood](https://www.unlockfood.ca/fr/Articles/Vegetarisme/Quatre-mesures-a-prendre-pour-avoir-un-regime-vegetalien-equilibre.aspx)

En plus de tout, cela évite beaucoup de soucis de santé.
Et cela permet encore une fois de réaliser des économies d'argent.

Selon [planete healthy](https://planetehealthy.com/reduire-consommation-viande/), voici les étapes pour réduire notre consommation de viande

1. Commencer par des petites décisions simples
2. Diminuer progressivement
3. Explorer de nouvelles saveurs
4. Organiser un repas-partage sans viande
5. Substituer avec des haricots et des lentilles
6. Être indulgent envers vous-memes
7. Choisir correctement ses objectifs
8. Ré-agençer votre garde-manger


### Réduire l'achat de vêtements.
Avant d'entamer mon année "0 achat de vêtements", je me suis demandé si j'avais porté __au moins une fois__ chacun de mes vêtements au cours des 12 derniers mois. Et la réponse était claire : *Non pas du tout*.
Je me suis rendu compte que je ne porte que les mêmes __3 chemises, 3 polos, 4 T-shirts, 2 pantalons, 2 paires de chaussures, 2 vestes__. J'ai pris conscience que je prends beaucoup plus de plaisir à essayer des vêtements dans les magasins et à m'imaginer à quelles soirées ou à quels évènements je pourrais les porter plutôt qu'à les porter réellement.

On a tous une chemise fétiche, ou un pantalon qui nous va comme un gant et qu'on choisis de porter parce qu'on l'aime.
Pour moi c'est vraiment une étape difficile de la décroissance car notre vêtement est la première chose que l'on voit de nous avant même d'avoir ouvert la bouche. Cela necessite d'avoir l'estime de sois bien accroché pour prendre le (petit) risque de ne pas être à la dernière mode.

Bon en réalité, une fois cette étape de courage franchis, j'ai pris un énorme plaisir à trier mes vêtements et à donner à emmaüs ceux qui ne me vont plus ou ceux que je n'aime pas porter. J'aime savoir que mes vêtements sont portés par quelqu'un plutôt qu'ils dorment au fond de ma penderie. Et je me sens beaucoup mieux à être recentré sur mes réelles envies. Je n'aurai jamais pensé ressentir autant de liberté après un simple tri de mes vêtements. Comme un effet magique de "nettoyage" psychique, vraiment, essayez vous verrez !

Je pense que ces notions de tri et de prise de décision sont liées à la sobriété qui nous rend fondamentalement heureux.

Stop aux achats compulsifs qui entretiennent notre angoisse et qui nous maintiennent dans l'illusion que parce qu'on aura des beaux et nouveaux vêtements, on se sentira mieux, plus attirant et qu'on va obtenir plus d'attention de la part des autres.

C'est là qu'on voit toute la puissance de frappe des publicités depuis des dizaines d'années ! C'est fort hein !?

Le marché de l'occasion du vêtement est florissant, beaucoup de marchés apparaissent tel que [Vinted](https://www.vinted.com/), [Kiwiiz](https://www.kiwiiz.fr/) ou [De filou en filou](https://www.defilouenfilou.fr/) pour les enfants.

### Diminuer les achats sur amazon.
Amazon, Alibaba, dealextrem ebay, ubaldi etc (la livraison amazon en 24 heures est une horreur en terme de rendement cargaison / pollution générée, sans parler qu'à chaque employé amazon équivaut 2,5 emplois supprimés)

### Chauffer à 19°C maximum.
19°C dans les pièces de vies.
16°C dans les chambres.
Ainsi, on attrape moins facilement le rhum en sortant de chez sois, puisqu'on est habitué à une température plus proche de celle dehors. Economie d'argent, de temps et de bien-être.

### Mutualiser les déplacements.
pour se rendre à une séance de sport, en soirée, en repas de famille)


